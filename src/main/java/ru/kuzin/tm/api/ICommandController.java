package ru.kuzin.tm.api;

public interface ICommandController {

    void showErrorCommand();

    void showSystemInfo();

    void showErrorArgument();

    void showVersion();

    void showAbout();

    void showHelp();

}
