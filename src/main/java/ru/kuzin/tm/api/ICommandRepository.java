package ru.kuzin.tm.api;

import ru.kuzin.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
